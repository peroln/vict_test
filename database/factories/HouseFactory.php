<?php
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\House::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween( 1000, 1000000),
        'bedrooms' => $faker->numberBetween(1, 10),
        'bathrooms' => $faker->numberBetween(1, 5),
        'storeys' => $faker->numberBetween(1, 5),
        'garages' => $faker->numberBetween(1, 3),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,

    ];
});