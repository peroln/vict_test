<?php

namespace App\Http\Controllers\API;

use App\House;
use App\Http\Resources\House as HouseResource;
use App\Repository\HouseRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    protected $house;

    public function __construct(HouseRepositoryInterface $house)
    {
        $this->house = $house;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        //        return HouseResource::collection(House::SearchHouses($request));
        //        return HouseResource::collection(House::filter($request)->paginate(config('settings.paginate')));

        return HouseResource::collection($this->house->filter($request)->paginate(config('settings.paginate')));

    }

}
