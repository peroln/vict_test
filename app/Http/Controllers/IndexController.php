<?php

namespace App\Http\Controllers;

use App\House;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{
   public function index(){

       return Cache::store('memcached')->remember('view', 10, function(){
           $houses = House::paginate(config('settings.paginate')) ;
           return View::make('welcome', compact('houses'))->render();
       });

   }
}
