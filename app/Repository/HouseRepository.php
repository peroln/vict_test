<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 29.04.2019
 * Time: 11:47
 */

namespace App\Repository;


use App\House;
use Illuminate\Http\Request;

class HouseRepository implements HouseRepositoryInterface
{
    protected $house;

    /**
     * HouseRepository constructor.
     * @param House $house
     */
    public function __construct(House $house)
    {
        $this->house = $house;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function filter(Request $request){
       return $this->house->filter($request);
    }
}