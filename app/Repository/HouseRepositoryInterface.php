<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 29.04.2019
 * Time: 11:49
 */

namespace App\Repository;


use Illuminate\Http\Request;

interface HouseRepositoryInterface
{
    public function filter(Request $request);
}