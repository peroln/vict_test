<?php

namespace App\Providers;

use App\Repository\HouseRepository;
use App\Repository\HouseRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class HouseRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HouseRepositoryInterface::class, HouseRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
