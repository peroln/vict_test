<?php

namespace App;

use App\Filters\HouseFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class House extends Model
{
    /**
     * @param Request $request
     * @return mixed
     */
    public static function SearchHouses(Request $request){

//        $subRequest = DB::table('Houses');
        $subRequest = House::query();

        if($request->filled('name')){
            $subRequest->where('name','like','%'.$request->input('name').'%');
        }
        if($request->filled('id')){
            $subRequest->where('id', $request->input('id'));
        }
        if($request->filled('bedrooms')){
            $subRequest->where('bedrooms',$request->input('bedrooms'));
        }
        if($request->filled('bathrooms')){
            $subRequest->where('bathrooms',$request->input('bathrooms'));
        }
        if($request->filled('storeys')){

            $subRequest->where('storeys',$request->input('storeys'));
        }
        if($request->filled('garages')){
            $subRequest->where('garages',$request->input('garages'));
        }
        if($request->filled('price')){
            $price = $request->input('price');
            if(is_array($price)){
                $subRequest->whereBetween('price', [$price[0],$price[1]]);
            }
        }

        return $subRequest->paginate(config('settings.paginate'));
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder){

//        return  (new HouseFilter($request))->filter($builder);
       return app()->make(HouseFilter::class)->filter($builder);

    }
}
