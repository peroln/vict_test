<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 17.04.2019
 * Time: 17:33
 */

namespace App\Filters;


use App\House;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class AbstractFilter
{
    private $request;
    protected $filters = [];


    public function __construct(Request $request)
    {
        $this->request = $request;


    }


    /**
     * filter(Builder $builder) -> стартоый метод для создания фильтра
     *
     * @param Builder $builder
     * @return Builder
     */
    public function filter(Builder $builder)
    {

        // пробегаем по всем реквестам для фильтрации, формируя запрос в бд
        foreach ($this->getFilters() as $filter => $value) {
            $this->resolveFilter($filter)->filter($builder, $value);

        }

        return $builder;
    }


    /**
     * getFilters() -> получение данных запроса для фильтрации типа Key => value
     *
     * @return array
     */
    protected function getFilters()
    {
        // $this->filters -> получение классов параметров фильтрации из класса модели
        return array_filter($this->request->only(array_keys($this->filters)));
    }


    /**
     * resolveFilter($filter) -> получение объекта соответствующего запроса по ключу (поле в бд)
     *
     * @param $filter
     * @return mixed
     */
    protected function resolveFilter($filter)
    {
        return new $this->filters[$filter];
    }
}