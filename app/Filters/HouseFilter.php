<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 17.04.2019
 * Time: 17:39
 */

namespace App\Filters;


use App\Filters\Houses\Bathrooms;
use App\Filters\Houses\BedroomsFilter;
use App\Filters\Houses\Garages;
use App\Filters\Houses\NameFilter;
use App\Filters\Houses\Price;
use App\Filters\Houses\Storeys;

class HouseFilter extends AbstractFilter
{
    const FIELD_NAME = 'name';
    const FIELD_BEDROOMS = 'bedrooms';
    const FIELD_BATHROOMS = 'bathrooms';
    const FIELD_STOREYS = 'storeys';
    const FIELD_GARAGES = 'garages';
    const FIELD_PRICE = 'price';


    protected $filters = [
        self::FIELD_NAME => NameFilter::class,
        self::FIELD_BEDROOMS => BedroomsFilter::class,
        self::FIELD_BATHROOMS => Bathrooms::class,
        self::FIELD_STOREYS => Storeys::class,
        self::FIELD_GARAGES => Garages::class,
        self::FIELD_PRICE => Price::class
    ];
}