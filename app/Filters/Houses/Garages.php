<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 18.04.2019
 * Time: 14:15
 */

namespace App\Filters\Houses;


use App\Filters\HouseFilter;
use Illuminate\Database\Eloquent\Builder;

class Garages
{
    public function filter(Builder $builder, $value)
    {
        return $builder->where(HouseFilter::FIELD_GARAGES, $value);
    }
}