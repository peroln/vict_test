<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 18.04.2019
 * Time: 14:18
 */

namespace App\Filters\Houses;


use App\Filters\HouseFilter;
use Illuminate\Database\Eloquent\Builder;

class Price
{
    public function filter(Builder $builder, $value)
    {
        if(is_array($value)){
            $builder->whereBetween(HouseFilter::FIELD_PRICE, [$value[0],$value[1]]);
        }
    }

}