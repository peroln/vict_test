<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 17.04.2019
 * Time: 20:27
 */

namespace App\Filters\Houses;


use App\Filters\HouseFilter;

class BedroomsFilter
{
    public function filter($builder, $value)
    {
        return $builder->where(HouseFilter::FIELD_BEDROOMS, $value );

    }
}