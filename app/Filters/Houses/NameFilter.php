<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 17.04.2019
 * Time: 17:27
 *
 * NameFilter class is responsible for filtering the data based on the type.
 */

namespace App\Filters\Houses;


use App\Filters\HouseFilter;

class NameFilter
{
    public function filter($builder, $value)
    {
        return $builder->where(HouseFilter::FIELD_NAME, 'like', '%'.$value.'%' );

    }
}